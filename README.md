# MSSQL-Polybase-ML
MSSQL Server 2019 based on (old) Ubuntu 18.04 with Polybase and ML Services. Intended for use in DB course of UCN.

This image uses supervisord to run two services in one container, so the terminal output is completely garbled. Proper log output can be read from logfiles in `/tmp`. Startup usually takes 10-15 seconds, after which the output should die down.

## Using the image
```shell
docker pull registry.gitlab.com/hcrs/mssql-polybase-ml
docker run --rm -it registry.gitlab.com/hcrs/mssql-polybase-ml
```

## Building your own
```shell
git clone https://gitlab.com/hcrs/mssql-polybase-ml
cd mssql-polybase-ml
docker build -t mssql-polybase-ml .
docker run --rm -it mssql-polybase-ml
```

