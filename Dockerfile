FROM mcr.microsoft.com/mssql/server:2019-latest
USER root

# Tell aptitude that this is a scripted context
ENV DEBIAN_FRONTEND=noninteractive
# Set mssql version to Developer and accept EULA
ENV MSSQL_PID=Developer
ENV ACCEPT_EULA=Y
ENV ACCEPT_EULA_ML=Y

RUN apt-get update &&\
    apt-get install -y gnupg2 \
                       apt-transport-https \
                       curl \
                       supervisor \
                       fakechroot \
                       locales \
                       iptables \
                       sudo \
                       wget \
                       curl \
                       zip \
                       unzip \
                       make \ 
                       bzip2 \ 
                       m4 \
                       apt-transport-https \
                       tzdata \
                       libnuma-dev \
                       libsss-nss-idmap-dev \
                       software-properties-common

ADD https://packages.microsoft.com/keys/microsoft.asc /tmp/microsoft.asc
ADD https://packages.microsoft.com/config/ubuntu/18.04/mssql-server-2019.list /etc/apt/sources.list.d/mssql-server-2019.list
RUN apt-key add /tmp/microsoft.asc

RUN apt-get update && \
    apt-get install -y mssql-mlservices-packages-py \
                       mssql-server-polybase && \
    # Cleanup the Dockerfile
    apt-get clean && \
    rm -rf /var/apt/cache/* /tmp/* /var/tmp/* /var/lib/apt/lists

RUN addgroup mssql && adduser mssql mssql

# run checkinstallextensibility.sh
RUN /opt/mssql/bin/checkinstallextensibility.sh && \
    # set/fix directory permissions and create default directories
    chown -R root:root /opt/mssql/bin/launchpadd && \
    chmod 755 /opt/mssql/bin/launchpadd && \
    chown -R root:root /opt/mssql/bin/setnetbr && \
    chmod 755 /opt/mssql/bin/setnetbr && \
    mkdir -p /var/opt/mssql-extensibility/data && \
    mkdir -p /var/opt/mssql-extensibility/log && \
    chown -R root:root /var/opt/mssql-extensibility && \
    chmod -R 777 /var/opt/mssql-extensibility && \
    chmod -R 775 /var/opt/mssql && \
    chown -R mssql:mssql /var/opt/mssql && \
    mkdir -m 775 /.system && \
    chown -R mssql:mssql /.system && \
    # locale-gen
    locale-gen en_US.UTF-8

COPY ./supervisord.conf /usr/local/etc/supervisord.conf

# expose SQL Server port
EXPOSE 1433

# start services with supervisord
CMD /usr/bin/supervisord -n -c /usr/local/etc/supervisord.conf


